<?php

use Illuminate\Database\Seeder;

class VisibilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Visibility::truncate();

        DB::table('visibilities')->insert([

            'visibilityName' => 'Personal'
        ]);

        DB::table('visibilities')->insert([

            'visibilityName' => 'Public'
        ]);
    }
}
