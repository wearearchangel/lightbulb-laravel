<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Category::truncate();

        DB::table('categories')->insert([

            'title' => 'Art',
            'description' => 'ArtBased'

        ]);
    }
}
