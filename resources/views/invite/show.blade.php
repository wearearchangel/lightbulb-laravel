<div role="tabpanel">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#sentInvites" aria-controls="sentInvites" role="tab" data-toggle="tab">Sent</a></li>
        <li role="presentation" class="active"><a href="#receivedInvites" aria-controls="receivedInvites" role="tab" data-toggle="tab">Received</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="sentInvites">


            @if($invitesSent->count())

            <ul>
                @foreach($invitesSent as $invite)

                    <li class="list-unstyled">To {{ $user->to($invite->invited_id)->name }} - {{$invite->created_at->diffForHumans()}} </li>

                @endforeach
            </ul>

            @else
                No sent
            @endif


        </div>
        <div role="tabpanel" class="tab-pane active" id="receivedInvites">

            @if($invitesReceived->count())

            <ul>
                <table class="table">
                @foreach($invitesReceived as $invite)

                    <tr>
                        <td>From</td>
                        <td>Time</td>
                        <td>Team</td>
                        <td>Action</td>
                    </tr>
                    <tr>
                        <td>{{ $invite->user->name }}</td>
                        <td> {{$invite->created_at->diffForHumans()}}</td>
                        <td>{{ $teamClass->teamName($invite->team_id)->name }}</td>
                        <td>
                            <form method="post" action="{{ route('acceptInviteAndJoinTeam', [$invite->team_id, $invite->id]) }}" >

                              {{ csrf_field() }}
                              <button class="btn btn-info" type="submit">Accept</button>

                            </form>
                    </tr>

                    @endforeach
                </table>
            </ul>

            @else
                No received
            @endif

        </div>
    </div>

</div>