<li class="list-group-item list-unstyled" data-id="{{ $comment->id }}">
    @if(\Auth::user()->id == $comment->user->id)
    <span class="badge">
        <span class="deleteItem" aria-hidden="true">delete</span>
    </span>
    @endif
    @if(\Auth::user()->id == $comment->user->id)
     Me
    @else
     {{ $comment->user->name}}
    @endif
    {{ $comment->comment }} -
    ({{ $comment->created_at->diffForHumans() }})

</li>