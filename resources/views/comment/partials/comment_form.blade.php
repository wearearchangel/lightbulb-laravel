<div class="collapse comment" id="collapseCommentDiv_{{$idea->id}}" data-id="{{$idea->id}}">

    <div id="commentsList_{{$idea->id}}">
    </div>
    <div id="response_{{$idea->id}}"></div>
    <form id="{{$idea->id}}">
        <div class="form-group">
            <input type="text" name="comment" class="form-control" required>
        </div>

        <button type="submit" class="btn btn-info">Comment</button>
    </form>
</div>
