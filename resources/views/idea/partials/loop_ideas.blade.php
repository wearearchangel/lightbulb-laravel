@if($ideas->count())

    @foreach($ideas as $idea)

    <div>
        <div class="well well-lg" id="well_{{$idea->id}}">

            @if(Request::path() == 'ideas')
                <p class="text-uppercase">{{ $idea->title }}</p>
                <p>{{ $idea->description }}</p>
                <p class="text-primary">Posted by: {{ $idea->user->name }}</p>
                <p class="text-primary">about: {{ $idea->created_at->diffForHumans() }}</p>
            @endif


            @if(Request::path() == 'ideas/personal/all')

            <p class="text-uppercase">{{ $idea->title }}</p>
            <p>{{ $idea->description }}</p>
            <p class="text-primary">Posted by: {{ $idea->user->name }}</p>
            <p class="text-primary">about: {{ $idea->created_at->diffForHumans() }}</p>

            @if($idea->shareGroup != null)

            <p class="text-primary">Shared with: {{ $teamClass->teamName($idea->shareGroup)->name }}</p>

            @include('vote.index')

                @include('comment.partials.comment_system')

                @endif

            @endif

            @if(Request::is('ideas/shared/*'))

            <p class="text-uppercase">{{ $idea->title }}</p>
            <p>{{ $idea->description }}</p>
            <p class="text-primary">Posted by: {{ $idea->user->name }}</p>
            <p class="text-primary">about: {{ $idea->created_at->diffForHumans() }}</p>

            <div id="votes_{{$idea->id}}">

            </div>

                @include('vote.index')

                @include('comment.partials.comment_system')

            @endif






        </div>

    </div>

    @endforeach

@else

    <p>No Ideas here</p>

@endif

