<form method="post" action="{{ route('postIdea') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Title*</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="title" value="{{ old('title') }}">

            @if ($errors->has('title'))
                   <span class="help-block">
                         <strong>{{ $errors->first('title') }}</strong>
                    </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Description*</label>

        <div class="col-md-6">
            <textarea class="form-control" name="description"></textarea>

            @if ($errors->has('description'))
                     <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                     </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('ideaVisibility') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Visibility*</label>

        <div class="col-md-6">
            @foreach($visibility as $visible)
            <div class="radio">
                <label>
                    <input type="radio" name="ideaVisibility" id="idea_personal" value="{{ $visible->id }}">
                    {{$visible->visibilityName}}
                </label>
            </div>

            @endforeach

            @if ($errors->has('ideaVisibility'))
                 <span class="help-block">
                        <strong>{{ $errors->first('ideaVisibility') }}</strong>
                 </span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('shareGroup') ? ' has-error' : '' }}">

        <label class="col-md-4 control-label">Share with</label>

        <div class="col-md-6">

            <select class="form-control" name="shareGroup">

                <option selected disabled>Select team </option>

                @if($teams->count())
                @foreach($teams as $team)
                <option value="{{ $team->team_id }}">{{ $teamClass->teamName($team->team_id)->name }}</option>
                @endforeach
                @else
                <option>No groups around</option>
                @endif

            </select>
            @if ($errors->has('shareGroup'))
                     <span class="help-block">
                         <strong>{{ $errors->first('shareGroup') }}</strong>
                     </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-6">
            <button type="submit" class="btn btn-primary btn-group-lg">
                <i class="fa fa-btn fa-sign-in"></i>Save Idea
            </button>
        </div>
    </div>
</form>
