@extends('layouts.app')

@section('content')

<div class="container spark-screen">
    <div class="row">

        @include('flash.flash_message')


        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Members</div>

                <div class="panel-body">
                    @include('team.partials._members')
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
