
        <tr>
            <td>{{ $team->name }}</td>
            <td>{{ $team->description }}</td>
            <td>{{ $team->user->name }}</td>
            <td>{{ $team->created_at->diffForHumans() }}</td>
            <td>
                @if(Request::path() != 'teams')
                @if($members->count())
                    @include('team.partials._members')
                @endif
                @else
                <a href="{{ route('teamMembers', [$team->id]) }}">Members</a>
                @endif
            </td>

            @if(Request::path() != 'teams')
            <td>
                <a href="{{ route('teamIdeas', [$team->id]) }}">Ideas</a>
            </td>
            @endif

            <td>


                @if(\Auth::user()->isMember($team->id) == false)

                    <form method="post" action="{{ route('joinTeam', [$team->id]) }}" >

                @else
                    <form method="post" action="{{ route('leaveTeam', [$team->id]) }}" >

                @endif

                    {{ csrf_field() }}

                    @if(\Auth::user()->isMember($team->id) == false)
                        <button class="btn btn-info" type="submit">Join</button>
                    @else
                        <button class="btn btn-danger" type="submit">Leave</button>
                    @endif
                </form>

            </td>


        </tr>


