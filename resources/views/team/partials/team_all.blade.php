@if($teams->count())

    <table class="table">
        <tr>
            <td>Name</td>
            <td>Description</td>
            <td>Admin</td>
            <td>Created</td>
            <td>Members</td>
            <td>Action</td>

        </tr>
        @foreach($teams as $team)

            @include('team.partials.team')

        @endforeach
    </table>
@else

No teams

@endif