@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        @include('flash.flash_message')
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Team</div>

                <div class="panel-body">

                    @include('team.partials.add_new_team_form')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection