@extends('layouts.app')

@section('content')

<div class="container spark-screen">
    <div class="row">

        @include('flash.flash_message')

        <div class="col-md-1">
            <a href="{{ route('addNewTeam') }}">+Add team</a>
        </div>

        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Teams</div>

                <div class="panel-body">
                    @include('team.partials.team_all')
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
