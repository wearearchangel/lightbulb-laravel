@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        @include('flash.flash_message')
        @include('flash.flash_error')
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dashboard

                    @if(!Auth::guest())

                        <a class="col-md-offset-9" data-toggle="collapse" href="#collapseInviteDiv" aria-expanded="false" aria-controls="collapseInviteDiv">Send Invite Link</a>

                    @endif

                </div>

                <div class="panel-body">

                    @include('team.partials.team_invite_div')

                    <table class="table">
                            <tr>
                                <td>Name</td>
                                <td>Description</td>
                                <td>Admin</td>
                                <td>Created</td>
                                <td>Members</td>
                                <td>Ideas</td>
                                <td>Action</td>

                            </tr>
                        @include('team.partials.team')

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
