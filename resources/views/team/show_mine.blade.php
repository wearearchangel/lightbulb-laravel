 @extends('layouts.app')

 @section('content')
 <body class="cbp-spmenu-push">

      <button class="menu-button" id="menu-v1" style="
                z-index: 10000;
                
            "><span></span>
      </button>
      <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <h3>Menu</h3>
        <a href="home.html">Home</a>
        <a href="teams.html">Teams</a>
        <a href="#">Settings</a>
        <a href="login.html" id="log-out">Log Out</a>
      </nav>
    <div class="user">Brian Irungu</div>
    <div class="container">
    <div class="page-header">
      <h1 class="title">All Teams</h1>
    </div>

    <div class="row" style="min-height:600px;">
      <div  class="col-sm-12">
        <hr/>
        <div class="col-xs-3"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left">
            <li class="active"><a href="#home" data-toggle="tab">The Archangel Interactive</a></li>
            <li><a href="#profile" data-toggle="tab">Safaricom</a></li>
            <li><a href="#messages" data-toggle="tab">Mobicom Africa</a></li>
            <li><a href="#settings" data-toggle="tab">Team 4</a></li>
          </ul>
        </div>

        <div class="col-xs-9 ">
          <!-- Tab panes -->
          <div class="tab-content ">
            <div class="tab-pane active" id="home">
            	<div class="tabia">
	            	<h3 class="titular">Lorem Ipsum is an Idea</h3>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	            	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	            	quis nostrud exercitation </p> <br/ >
	            	<p class="crispy">Posted on dd/mm/yy<small> | Idea by</small></p>
	            </div>
	            <div class="tabia col-xs-offset-4">
	            	<h3 class="titular">Lorem Ipsum is an Idea</h3>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	            	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	            	quis nostrud exercitation </p> <br/ >
	            	<p class="crispy">Posted on dd/mm/yy<small> | Idea by</small></p>
	            </div>
	            <div class="tabia">
	            	<h3 class="titular">Lorem Ipsum is an Idea</h3>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	            	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	            	quis nostrud exercitation </p> <br/ >
	            	<p class="crispy">Posted on dd/mm/yy<small> | Idea by</small></p>
	            </div>
	            <div class="tabia col-xs-offset-4">
	            	<h3 class="titular">Lorem Ipsum is an Idea</h3>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	            	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	            	quis nostrud exercitation </p> <br/ >
	            	<p class="crispy">Posted on dd/mm/yy<small> | Idea by</small></p>
	            </div>
            </div>
            <div class="tab-pane" id="profile">            	
            	<div class="tabia">
	            	<h3 class="titular">Korem Ipsum is an Idea</h3>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	            	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	            	quis nostrud exercitation </p> <br/>
	            	<p class="crispy">Posted on dd/mm/yy<small> | Idea by</small></p>
	            </div>
	        </div>
            <div class="tab-pane" id="messages">
            	<div class="tabia">
	            	<h3 class="titular">Morem Ipsum is an Idea</h3>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	            	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	            	quis nostrud exercitation </p> <br/>
	            	<p class="crispy">Posted on dd/mm/yy<small> | Idea by</small></p>
	            </div>
            </div>
            <div class="tab-pane" id="settings">            	
            	<div class="tabia">
	            	<h3 class="titular">Norem Ipsum is an Idea</h3>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	            	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	            	quis nostrud exercitation </p> <br/>
	            	<p class="crispy">Posted on dd/mm/yy<small> | Idea by</small></p>
	            </div>
	        </div>
          </div>
        </div>

        <div class="clearfix"></div>

      </div>
    </div>

    <div class="clearfix"></div>

    <script src="js/classie.js"></script>
    <script>
      var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeft = document.getElementById( 'showLeft' ),
        menuOpen = document.getElementById( 'menu-v1' ),
        body = document.body;

      function disableOther( button ) {
        if( button !== 'showLeft' ) {
          classie.toggle( showLeft, 'disabled' );
        }
      }

      menuOpen.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        classie.toggle( menuOpen, 'menu-button--open' );
        disableOther( 'menuOpen' );
        disableOther( 'showLeft' );
      };

      function disableOther( button ) {
        if( button !== 'menu-v1' ) {
          classie.toggle( menuOpen, 'disabled' );
        }
      }

      
    </script>
    @endsection