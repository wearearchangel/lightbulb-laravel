<form class="form-horizontal" role="form" method="POST" action="">
    {!! csrf_field() !!}

    <div class="well">

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Name</label>

            <div class="col-md-6">

                @if($user->facebook_id == null)
                    <input type="text" class="form-control" name="name" value="{{$user->name}}">
                @else
                    <input type="text" class="form-control" name="name" value="{{$user->name}}" disabled>
                @endif

                @if ($errors->has('name'))
                 <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                 </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">E-Mail Address</label>

            <div class="col-md-6">
                @if($user->facebook == null)
                    <input type="email" class="form-control" name="email" value="{{$user->email}}">
                @else
                    <input type="email" class="form-control" name="email" value="{{$user->email}}" disabled>
                @endif

                @if ($errors->has('email'))
                  <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
            </div>
        </div>

    </div>


    @if($user->facebook_id == null)
    <div class="well">

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Current Password</label>

            <div class="col-md-6">
                <input type="password" class="form-control" name="password">

                @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">New Password</label>

            <div class="col-md-6">
                <input type="password" class="form-control" name="password">

                @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Confirm Password</label>

            <div class="col-md-6">
                <input type="password" class="form-control" name="password_confirmation">

                @if ($errors->has('password_confirmation'))
                <span class="help-block">
                     <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i>Register
                </button>
            </div>
        </div>
    </div>

    @endif


</form>
