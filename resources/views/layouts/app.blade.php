<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ asset('css/libs/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/iMS.css') }}" rel="stylesheet">
    <link href="{{ asset('css/default.css') }}" rel="stylesheet">
    <link href="{{ asset('css/component.css') }}" rel="stylesheet">
    <link href="{{ asset('css/clean.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.vertical-tabs.min.css') }}" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#spark-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Cloud Ims
                </a>
            </div>

            <div class="collapse navbar-collapse" id="spark-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @if(!Auth::guest())
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="{{ route('publicIdeas') }}">Public</a></li>

                        <li><a href="{{ route('personalIdeas')}}">Personal</a></li>

                    </ul>
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @if(!(Request::path() == 'account/settings/view'))
                                    <li><a href="{{ route('accountSettings') }}">Settings</a></li>
                                @endif
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="{{ asset('js/libs/jquery.min.js') }}"></script>
    <script src="{{ asset('js/libs/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/classie.js') }}"></script>
    <script src="{{ asset('js/iMS.js') }}"></script>
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>




    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script>
        $('div.alert-message').not('.alert-important').delay(4000).slideUp(300);
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $( 'meta[name="csrf-token"]' ).attr( 'content' )
            }
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>

    <script type="text/javascript">
        //variable definitions
        var show_tab = 'shown.bs.collapse';
        //active tab
        var tab;
        //clicked tab data-id
        var target;
        //renders messagess new state to the page
        function addComment( idea_id ) {
            $.get( "/comments/single/" + idea_id, function( data ) {
                $( ".comments_"+idea_id).append(data);
            });
        }

        function getComments(idea_id)
        {
            $.get( "/comments/"+idea_id, function( data ) {
                $( "#commentsList_"+idea_id).html( data );
            });
        }

        function voteForIdea(idea_id)
        {
            $.get( "/votes/"+idea_id, function( data ) {

                $( "#votes_"+idea_id).html( data );

                var button = $(document).find('#button_'+idea_id);

                button.detach();
            });
        }

        //removes item's old state from the page
        function removeComment( id ) {
            $( 'li[data-id="' + id + '"' ).remove();
        }
        //Identify opened tab and submits message to tab's thread
        $(function () {
            $('.comment').on('shown.bs.collapse', function (e) {

                tab = $(e.target);

                target = tab.attr('data-id');

                $('#response_'+target).text('Loading...');

                $.ajax( '/comments/'+target, {
                    method: 'GET',
                    cache: false,
                    success: function() {
                        $('#response_'+target).text('');

                        getComments(target);
                    }


                });

                var form = tab.find('form');

                form.on('submit', function(e)
                {
                    e.preventDefault()

                    $(this).find('#help-block_'+target).text('');

                    var idea_id = $('#collapseCommentDiv_'+target).attr('data-id');


                    $('#response_'+target).text('sending...');

                    $.ajax( '/comments/'+ idea_id + '/post', {
                        data: form.serialize(),
                        method: 'POST',
                        cache: false,
                        success: function() {
                            $('#response_'+target).text('');

                            addComment(idea_id);
                        }
                    });
                    form.find('input[type=text]').val('');
                });

                $( document ).on( "click", ".deleteItem", function() {
                    var id = $( this ).closest( 'li' ).data( 'id' );

                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this comment",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    },
                    function(){

                        $.ajax( '/comments/' + id+ '/delete', {
                            method: 'DELETE',
                            success: function() {

                                removeComment( id );
                            }
                        });

                    })
                });

            });

            $( document ).on( "click", ".voteForIdea", function() {
                var id = $( this ).closest( 'button' ).data( 'id' );

                $.ajax( '/votes/' + id+ '/vote', {
                    method: 'POST',
                    success: function() {

                        voteForIdea( id );

                    }
                });
            });
        });
    </script>
</body>
</html>
