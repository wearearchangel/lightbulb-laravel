@if (count($errors) > 0)
<div class="alert alert-danger" role="alert" >
    <h5>Error! <b>Check a few things up</b> and try submitting again!</h5>
</div>
@endif