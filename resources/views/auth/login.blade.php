@extends('layouts.app')

@section('content')

        <div id="login-form" class="form">
            <div id="auth_error" style="color:red; width: 300px; margin-right: auto; margin-left: auto;">.</div>

            <div class="container">
                  <div class="row">
                    <div class ="login">
                      <div class="col-md-offset-3 col-md-6">
                      <h2 class="title">Login</h2>
                        <form role="form" method="POST" action="{{ url('/login') }}">
                            {!! csrf_field() !!}

                          <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" name='email' placeholder="Email" class="form-control" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                          </div>
                          <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" name='password' placeholder="Password" class="form-control">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                          </div>
                           <div class="loginbox">
                            <label><input type="checkbox"><span>Remember me</span></label>
                            <button type='submit' class="btn signin-btn" type="button" name="remember">LOGIN</button>
                          </div>                    
                        </form>
                      </div>
                    </div>
                  </div>
            </div> 
        </div>


<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i>Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </form>
                    <br>
                    <br>
                    <div class="col-md-offset-4">
                        <a href="{{ route('redirectToProvider')}}" class="btn btn-info" role="button">Login with Facebook</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
