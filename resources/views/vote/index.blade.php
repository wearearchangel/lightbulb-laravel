<div>

    @if(\Auth::user()->hasVoted($idea->id) == true)

        <button class="btn btn-info btn-sm" disabled>{{ $idea->votes()->count() }} Votes</button>

    @else

        <button type="submit"  id="button_{{$idea->id}}" data-id="{{$idea->id}}" class="btn btn-info btn-sm voteForIdea">{{ $idea->votes()->count() }} Votes</button>

    @endif


</div>
