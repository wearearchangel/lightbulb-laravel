@extends('layouts.app')

@section('content')
<button class="menu-button eff" id="menu-v1" style="
                z-index: 10000;
                
            "><span></span></button>
            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                <h3>Menu</h3>
                <a href="home.html">Home</a>
                <!-- <a href="about.html">About iMS</a> -->
                <a href="{{ route('getMyTeams')}}">My Teams</a>
                <a href="{{ route('getAllTeams')}}">All Teams</a>

                <a href="#">Settings</a>
                <!-- <a href="#">Contact</a> -->
                <!-- <a href="blog.html">Blog</a> -->
                <a href="index.php?logout=true" id="log-out">Log Out</a>
            </nav>

            <!-- main -->
                <!-- <div class="user">Brian Irungu</div> -->
                <div class="row tropez">

                    <!-- columns -->
                    <div class="col-lg-3">
                        
                    </div> <!-- end col-lg-3 -->

                    <div class="col-lg-6">
                       <div class="img-clip-block">
                            <div class="img-clip-row">
                                <div class="img-clip-wrap">
                                </div>
                            </div>

                            <div class="img-clip-row">
                                <a href="personal.html" class="img-clip-wrap">
                                    <div class="overlay">
                                        <div class="overlay-content">Personal</div>
                                    </div>
                                </a>

                                <a href="teams.html" class="img-clip-wrap">
                                    <div class="overlay">         
                                        <div class="overlay-content">T.A.I</div>
                                    </div>
                                </a>
                            </div>

                            <div class="img-clip-row">
                                <a href="public.html" class="img-clip-wrap polygon-clip-rhombus">
                                    <div class="overlay">
                                        <div class="overlay-content">Public</div>            
                                    </div>
                                </a>
                            </div>
                        </div> <!-- end of img-clip-block -->

                        <svg class="clip-svg">
                          <defs>
                                <clipPath id="clip-diamond-demo" clipPathUnits="objectBoundingBox">
                                      <polygon points="0.5 0, 1 0.5, 0.5 1, 0 0.5" />
                                </clipPath>
                          </defs>   
                        </svg>  
                    </div> <!-- end-col-lg-6 -->

                    <div class="">
                        
                        <div class="advert well"></div>
                        <div class="col-lg-3 trap">
                            <div class="funded-projects">                            
                                <a href=""></a>
                                <a class="modal-form" data-target="#create-form" data-toggle="modal" href="#"><button class="btn signin-btn">Write New Idea</button></a>
                            </div>              
                        </div> <!-- end col-lg-3 -->
                    </div> 
                    <!-- Start create modal window -->
                      <div aria-hidden="false" role="dialog" tabindex="-1" id="create-form" class="modal leread-modal fade in">
                        <div class="modal-dialog">
                          <!-- Start create section -->
                          <div id="create-content" class="modal-content">
                            <div class="modal-header">
                              <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                              <h2 class="modal-title title"></i>Write New Idea</h2>
                            </div>
                            <div class="modal-body">
                              <div id="auth_error" style="color:red; width: 300px; margin-right: auto; margin-left: auto;">d.</div>
                              <form action="auth.php" method="post" id="new_idea_form">
                                <div class="form-group">
                                  <textarea name="idea"></textarea>
                                  <button class="btn signin-btn" type="button">Save</button>
                                </div>
                                <div class="form-group">
                                    <span><input type="radio" aria-label="..." name="shared" checked></span><p>Personal</p>
                                    <span><input type="radio" aria-label="..." name="shared"></span><p>Public</p>
                                    <span><input type="radio" aria-label="..." name="shared" ></span><p>T.A.I</p>
                                </div>                    
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- End create modal window -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="">
                        
                    </div>
                </div>

    
        <script src="js/classie.js"></script>
        <script>
            var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
                showLeft = document.getElementById( 'showLeft' ),
                menuOpen = document.getElementById( 'menu-v1' ),
                body = document.body;

            function disableOther( button ) {
                if( button !== 'showLeft' ) {
                    classie.toggle( showLeft, 'disabled' );
                }
            }

            menuOpen.onclick = function() {
                classie.toggle( this, 'active' );
                classie.toggle( menuLeft, 'cbp-spmenu-open' );
                classie.toggle( menuOpen, 'menu-button--open' );
                disableOther( 'menuOpen' );
                disableOther( 'showLeft' );
            };

            function disableOther( button ) {
                if( button !== 'menu-v1' ) {
                    classie.toggle( menuOpen, 'disabled' );
                }
            }
        </script>



<div class="container spark-screen">
    <div class="row">

        @include('flash.flash_message')

        <div class="col-md-1">

            My Teams

            @if($teams->count())

                @foreach($teams as $team)

                    <ul class="list-unstyled">
                        <li><a href="{{ route('getTeam', [$team->team_id]) }}">{{ $teamClass->teamName($team->team_id)->name }}</a></li>
                    </ul>
                @endforeach
            @else

               <p class="text-info">You have no teams</p>

            @endif

            <a href="{{ route('getAllTeams') }}" class="text-info">All Teams</a>

        </div>

        <div class="col-md-1">
            <a href="{{ route('addNewTeam') }}">+Add team</a>
        </div>

        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">New Idea</div>

                <div class="panel-body">
                   @include('idea.partials.idea_post_form')
                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Team Invites</div>

                <div class="panel-body">
                    @include('invite.show', [$teamClass])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
