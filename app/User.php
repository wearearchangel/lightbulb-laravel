<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'facebook_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * User Idea relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function idea()
    {
        return $this->hasMany(Idea::class);
    }

    /**
     * User Team Relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function team()
    {
        return $this->hasMany(Team::class);
    }

    /**
     * User invite relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invite()
    {
        return $this->hasMany(Invite::class);
    }

    public function user_teams()
    {
        return $this->hasMany(User_teams::class);
    }

    public function isMember($team_id)
    {
        if($this->user_teams()->where('team_id', '=', $team_id)->first() == null)
        {
            return false;
        }

        return true;
    }

    public function hasVoted($idea_id)
    {
        if($this->vote()->where('idea_id', '=', $idea_id)->first() == null)
        {
            return false;
        }

        return true;
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function vote()
    {
        return $this->hasOne(Vote::class);
    }

}
