<?php

namespace App;

use Faker\Test\Provider\TestableBarcode;
use Illuminate\Database\Eloquent\Model;

class User_teams extends Model
{
    protected $table = 'user_teams';

    protected $fillable = [

        'team_id',
        'user_id'

    ];

    public function teams()
    {
        return $this->hasOne(Team::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function idea()
    {
        return  $this->hasMany(Idea::class, 'shareGroup');
    }
}
