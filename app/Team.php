<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'name',
        'description'
    ];

    /**
     * User Team relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function user_teams()
    {
        return $this->hasMany(User_teams::class);
    }

    public function idea()
    {
        return $this->hasMany(Idea::class, 'shareGroup');
    }


}
