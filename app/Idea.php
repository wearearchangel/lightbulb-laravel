<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    /**
     * All the fields that can be mass assigned
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'category_id',
        'ideaVisibility',
        'shareGroup'
    ];

    /**
     * Ideas User Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Category Idea relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category()
    {
        return $this->belongsTo(Idea::class);
    }

    public function user_team()
    {
        return $this->belongsTo(User_teams::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

}
