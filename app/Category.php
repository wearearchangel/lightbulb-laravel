<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * All the fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'title',
        'description'
    ];

    /**
     * Idea Category relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function idea()
    {
        return $this->hasMany(Idea::class);
    }
}
