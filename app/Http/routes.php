<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'HomeController@index');

    Route::auth();

    Route::get('home', [

        'as'=> 'home',
        'uses' => 'HomeController@index'
    ]);


});

Route::group(['middleware' => 'web', 'prefix' => 'ideas', 'before' => 'csrf'], function () {

    Route::group(['middleware' => 'auth'], function() {
        Route::get('/', ['as' => 'publicIdeas', 'uses' => 'Ideas\IdeasController@publicIdeas']);
        Route::get('/personal/all', ['as' => 'personalIdeas', 'uses' => 'Ideas\IdeasController@personalIdeas']);
        Route::get('/shared/{team_id}', ['as' => 'teamIdeas', 'uses' => 'Ideas\IdeasController@teamIdeas']);
        Route::get('/{idea_id}/vote', ['as' => 'voteForIdea', 'uses' => 'Ideas\IdeasController@voteUp']);
        Route::post('/store', ['as' => 'postIdea', 'uses' => 'Ideas\IdeasController@store']);
    });
});

Route::group(['middleware' => 'web', 'prefix' => 'votes', 'before' => 'csrf'], function () {

    Route::group(['middleware' => 'auth'], function() {

        Route::post('/{idea_id}/vote', ['as' => 'voteForIdea', 'uses' => 'Votes\VotesController@store']);
        Route::get('/{idea_id}', ['as' => 'getVotes', 'uses' => 'Votes\VotesController@index']);
    });
});

Route::group(['middleware' => 'web', 'prefix' => 'comments', 'before' => 'csrf'], function () {

    Route::group(['middleware' => 'auth'], function() {
        Route::get('{idea_id}/', ['as' => 'allComments', 'uses' => 'Comments\CommentsController@index']);
        Route::post('/{idea_id}/post', ['as' => 'postComment', 'uses' => 'Comments\CommentsController@store']);
        Route::get('/single/{idea_id}', ['as' => 'getComment', 'uses' => 'Comments\CommentsController@show']);
        Route::delete('{comment_id}/delete', ['as' => 'deleteComment', 'uses' => 'Comments\CommentsController@destroy']);
    });
});

Route::group(['middleware' => 'web', 'prefix' => 'teams', 'before' => 'csrf'], function () {

    Route::group(['middleware' => 'auth'], function() {

        Route::get('/', ['as' => 'getAllTeams', 'uses' => 'Teams\TeamsController@getAll']);

        Route::get('/new/add', ['as' => 'addNewTeam', 'uses' => 'Teams\TeamsController@addNew']);

        Route::get('/members/{team_id}', ['as' => 'teamMembers', 'uses' => 'Teams\TeamsController@members']);

        Route::get('single/{team_id}', ['as' => 'getTeam', 'uses' => 'Teams\TeamsController@getTeam']);

        Route::get('/mine', ['as' => 'getMyTeams', 'uses' => 'Teams\TeamsController@getMyTeams']);


        Route::post('/store', ['as' => 'saveTeam', 'uses' => 'Teams\TeamsController@store']);

        Route::post('/{team_id}/join', ['as' => 'joinTeam', 'uses' => 'Teams\TeamsController@join']);

        Route::post('/{team_id}/leave', ['as' => 'leaveTeam', 'uses' => 'Teams\TeamsController@leave']);

        Route::post('/accept/{team_id}/{invite_id}', ['as' => 'acceptInviteAndJoinTeam', 'uses' => 'Teams\TeamsController@accept']);


    });
});

Route::group(['middleware' => 'web', 'prefix' => 'account', 'before' => 'csrf'], function () {

    Route::group(['middleware' => 'auth'], function() {

        Route::get('/settings/view', ['as' => 'accountSettings', 'uses' => 'Accounts\AccountsController@getSettings']);

        Route::get('/change/default', ['as' => 'changeDefaultTeam', 'uses' => 'Accounts\AccountsController@changeDefaultTeam']);
    });
});

Route::group(['middleware' => 'web', 'prefix' => 'invites', 'before' => 'csrf'], function () {

        Route::group(['middleware' => 'auth'], function() {

        Route::post('/send/{team_id}', ['as' => 'sendInvitation', 'uses' => 'Invites\InvitesController@sendNow']);
    });
});

Route::group(['middleware' => 'web', 'prefix' => 'auth', 'before' => 'csrf'], function () {

        Route::get('facebook', [

            'as' => 'redirectToProvider', 'uses' => 'Auth\AuthController@redirectToProvider'
        ]);

        Route::get('facebook/callback', [

            'as' => 'facebookCallback', 'uses' => 'Auth\AuthController@handleProviderCallback'
        ]);
});
