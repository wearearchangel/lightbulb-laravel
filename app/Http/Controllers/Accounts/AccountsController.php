<?php

namespace App\Http\Controllers\Accounts;

use App\Repos\TeamRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountsController extends Controller
{
    public function getSettings()
    {

        $user = \Auth::user();

        return view('account.settings', compact('user'));
    }
}
