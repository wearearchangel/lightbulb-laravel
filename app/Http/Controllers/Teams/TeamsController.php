<?php

namespace App\Http\Controllers\Teams;

use App\Repos\InviteRepo;
use App\Team;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveTeamRequest;
use App\Repos\TeamRepo;
use Illuminate\Support\Facades\Session;
use App\Repos\UserRepo;

class TeamsController extends Controller
{
    /**
     * Return the add new team view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addNew()
    {
        return view('team.add_new');
    }

    /**
     * Save a new team
     * @param SaveTeamRequest $saveTeamRequest
     * @param TeamRepo $teamRepo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SaveTeamRequest $saveTeamRequest, TeamRepo $teamRepo)
    {
        $teamRepo->store($saveTeamRequest);

        Session::flash('flash_message', 'Team was added successfully');

        return redirect()->back();
    }

    public function getTeam(TeamRepo $teamRepo, UserRepo $userRepo, $team_id)
    {
        $team = $teamRepo->getTeam($team_id);

        $users = $userRepo->getAllExceptMe();

        $members = $teamRepo->members($team_id);

        return view('team.show', compact('team', 'users', 'team_id', 'members'));
    }

    public function getAll(TeamRepo $teamRepo)
    {
        $teams = $teamRepo->getAll();

        return view('team.index', compact('teams'));
    }

    public function getMyTeams(TeamRepo $teamRepo){

        $teams = $teamRepo->getMyTeams();

        $teamClass = new TeamRepo();

        return view('team.show_mine', compact('teams', 'teamClass'));
    }

    public function join(TeamRepo $teamRepo, $team_id)
    {
        $teamRepo->join($team_id);

        Session::flash('flash_message', 'You successfully joined the team');

        return redirect()->back();
    }

    public function leave(TeamRepo $teamRepo, $team_id)
    {
        $teamRepo->leave($team_id);

        Session::flash('flash_message', 'You successfully left the team');

        return redirect('home');
    }

    public function members(TeamRepo $teamRepo, $team_id)
    {
        $members = $teamRepo->members($team_id);

        return view('team.members', compact('members'));
    }

    public function accept(TeamRepo $teamRepo, InviteRepo $inviteRepo, $team_id, $invite_id)
    {
        $teamRepo->acceptAndJoin($team_id);

        $inviteRepo->clear($invite_id);

        Session::flash('flash_message', 'You successfully joined the team');

        return redirect()->back();
    }
}
