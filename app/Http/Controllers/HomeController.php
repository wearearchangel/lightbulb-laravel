<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repos\InviteRepo;
use App\Repos\TeamRepo;
use App\Repos\UserRepo;
use App\User;
use Illuminate\Http\Request;
use App\Repos\VisibilityRepo;

class HomeController extends Controller
{
    /**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param VisibilityRepo $visibilityRepo
     * @param TeamRepo $teamRepo
     * @param InviteRepo $inviteRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(VisibilityRepo $visibilityRepo, TeamRepo $teamRepo, InviteRepo $inviteRepo)
    {
        $teams = $teamRepo->getMyTeams()->get();

        $invitesSent = $inviteRepo->getSentForUser(\Auth::user()->id);

        $invitesReceived = $inviteRepo->getReceivedForUser(\Auth::user()->id);

        $visibility = $visibilityRepo->getAllVisibilities();

        $user = new UserRepo();

        $teamClass = new TeamRepo();

        return view('home', compact('visibility', 'teams', 'invitesSent', 'invitesReceived', 'user', 'teamClass'));
    }
}
