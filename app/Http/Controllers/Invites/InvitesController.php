<?php

namespace App\Http\Controllers\Invites;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendInviteRequest;
use App\Repos\InviteRepo;
use App\Repos\UserRepo;
use Illuminate\Support\Facades\Session;

class InvitesController extends Controller
{
    public function sendNow(SendInviteRequest $sendInviteRequest, InviteRepo $inviteRepo, UserRepo $userRepo, $team_id)
    {
        $user = $userRepo->getUserFromEmail($sendInviteRequest->email);

        $inviteRepo->invite($user, $team_id);

        Session::flash('flash_message', 'An Invite was successfully sent to ' . '<b>'.$user->name  .'</b>');

        return redirect()->back();
    }
}
