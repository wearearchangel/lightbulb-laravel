<?php

namespace App\Http\Controllers\Ideas;

use App\Idea;
use App\Repos\IdeaRepo;
use App\Repos\TeamRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostIdeaRequest;
use Illuminate\Support\Facades\Session;


class IdeasController extends Controller
{

    /**
     * @param IdeaRepo $ideaRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function personalIdeas(IdeaRepo $ideaRepo)
    {
        $ideas = $ideaRepo->getPersonal();

        $teamClass = new TeamRepo();

        return view('idea.show_personal_ideas', compact('ideas', 'teamClass'));
    }

    /**
     * @param IdeaRepo $ideaRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function publicIdeas(IdeaRepo $ideaRepo)
    {
        $ideas = $ideaRepo->getPublic();

        return view('idea.show_public_ideas', compact('ideas'));
    }


    /**
     * @param IdeaRepo $ideaRepo
     * @param TeamRepo $teamRepo
     * @param $team_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function teamIdeas(IdeaRepo $ideaRepo, TeamRepo $teamRepo, $team_id)
    {
        $ideas = $ideaRepo->getForTeam($team_id);

        //dd($ideas);


       $teamClass = new TeamRepo();
        return view('idea.show_team_ideas', compact('ideas', 'teamClass'));
    }


    /**
     * @param PostIdeaRequest $request
     * @param IdeaRepo $ideaRepo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PostIdeaRequest $request, IdeaRepo $ideaRepo)
    {
        $ideaRepo->persist($request);

        Session::flash('flash_message', 'Idea was successfully posted');

        return redirect('home');
    }
}
