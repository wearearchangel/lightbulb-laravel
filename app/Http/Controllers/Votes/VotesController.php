<?php

namespace App\Http\Controllers\Votes;

use App\Vote;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VotesController extends Controller
{

    public function index($idea_id)
    {
        $votes_count = Vote::where('idea_id', '=', $idea_id)
            ->count();

        return view('vote.show', compact('votes_count', compact('idea_id')));
    }

    public function store($idea_id)
    {
        Vote::create([
           'idea_id' => $idea_id,
           'user_id' => \Auth::user()->id
        ]);
    }
}
