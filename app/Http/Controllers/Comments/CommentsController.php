<?php

namespace App\Http\Controllers\Comments;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comment;
use phpDocumentor\Reflection\DocBlock\Type\Collection;
use App\Repos\CommentRepo;

class CommentsController extends Controller
{
    private $repo;

    public function __construct(CommentRepo $commentRepo){

        $this->repo = $commentRepo;

    }

    public function store(Request $request, $idea_id){
        $this->repo->store($request, $idea_id);
    }

    public function index($idea_id){
        $comments = $this->repo->forAnIdea($idea_id);

        return view('comment.index', compact('comments', 'idea_id'));
    }

    public function show($idea_id){

        $comment = $this->repo->show($idea_id);

        return view('comment.show', compact('comment', 'idea_id'));
    }

    public function destroy($comment_id){

        $this->repo->destroy($comment_id);
    }
}
