<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/22/16
 * Time: 4:22 PM
 */

namespace App\Repos;

use App\Team;
use App\User_teams;

class TeamRepo {

    /**
     * Save a new team
     * @param $request
     */
    public function store($request)
    {
        $team = \Auth::user()->team()
                     ->create([

                'name' =>  $request->name,
                'description' => $request->description
            ]);

        $this->join($team->id);
    }

    /**
     * Get all teams
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getMyTeams()
    {
       return User_teams::where('user_id', '=', \Auth::user()->id);
    }

    /**
     * Get a team
     * @param $team_id
     * @return mixed
     */
    public function getTeam($team_id)
    {
        return Team::findOrFail($team_id);
    }

    /**
     * Get authenticated user default team
     * @return mixed
     */
    public function getDefaultTeam()
    {
        return  \Auth::user()->team()->where('default', true)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return Team::all();
    }

    public function join($team_id)
    {
        \Auth::user()->user_teams()
                    ->create([

               'team_id' => $team_id

            ]);
    }

    public function acceptAndJoin($team_id)
    {
        \Auth::user()->user_teams()
            ->create([

                'team_id' => $team_id

            ]);

    }

    public function leave($team_id)
    {
        $user_team = \Auth::user()->user_teams()->where('team_id', '=', $team_id)->first();

        $user_team->delete();
    }

    public function teamName($team_id)
    {
        return Team::findOrFail($team_id);
    }

    public function members($team_id)
    {
        return User_teams::where('team_id', '=', $team_id)->with('user')->get();
    }



} 