<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/22/16
 * Time: 11:11 AM
 */

namespace App\Repos;

use App\Visibility;


class VisibilityRepo {

    /**
     * @var \App\Visibility
     */
    protected $model;

    /**
     * @param Visibility $visibility
     */
    public function __construct(Visibility $visibility)
    {
        $this->model = $visibility;
    }

    /**
     * Gets all idea visibilities levels
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllVisibilities()
    {
        return $this->model->all();
    }

} 