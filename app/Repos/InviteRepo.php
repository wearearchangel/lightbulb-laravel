<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/23/16
 * Time: 9:57 AM
 */

namespace App\Repos;
use App\Invite;
use Illuminate\Support\Facades\Auth;
use App\Repos\UserRepo;


class InviteRepo {

    /**
     * @param $user
     */
    public function invite($user, $team_id)
    {

        Auth::user()->invite()->create([

            'invited_id' => $user->id,
            'team_id'  => $team_id
        ]);
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getSentForUser($user_id)
    {
       return Invite::where('user_id', '=', $user_id)->get();
    }

    public function getReceivedForUser($user_id)
    {
        return Invite::where('invited_id', '=', $user_id)->get();
    }

    public function clear($invite_id)
    {
        $invite = Invite::findOrFail($invite_id);

        $invite->delete();
    }

} 