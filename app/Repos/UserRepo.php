<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/23/16
 * Time: 10:07 AM
 */

namespace App\Repos;

use App\User;


class UserRepo {
    /**
     * @param $email
     * @return mixed
     */
    public function getUserFromEmail($email)
    {
        return User::where('email', '=', $email)->first();
    }

    /**
     * Get all registered users
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllExceptMe()
    {
        return User::where('id', '!=', \Auth::user()->id)->get();
    }


    /**
     * @param $user_id
     * @return mixed
     */
    public function to($user_id)
    {
        return User::findOrFail($user_id);
    }

} 