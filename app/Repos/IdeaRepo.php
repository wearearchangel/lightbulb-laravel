<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/21/16
 * Time: 1:15 PM
 */

namespace App\Repos;
use App\Idea;
use App\User_teams;
use Illuminate\Support\Facades\DB;


class IdeaRepo {

    /**
     * Saves a new Idea to the db
     * @param $request
     */
    public function persist($request)
    {

      \Auth::user()->idea()->create([

          'title' => $request->title,
          'description' => $request->description,
          'category_id' => 1,
          'ideaVisibility' => $request->ideaVisibility,
          'shareGroup' => $request->shareGroup
      ]);
    }

    /**
     * @return mixed
     */
    public function getPersonal()
    {
        return Idea::where('user_id', '=', \Auth::user()->id)
                    ->where('ideaVisibility', '=', 1)
                    ->with('user')
                    ->get();
    }

    /**
     * @return mixed
     */
    public function getPublic()
    {
       return Idea::where('ideaVisibility', '=', 2)
                    ->with('user')
                    ->get();
    }

    /**
     * @return mixed
     */
    public function getForTeam($team_id)
    {
        return Idea::where('shareGroup', '=', $team_id)->get();
    }


} 