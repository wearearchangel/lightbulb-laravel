<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/1/16
 * Time: 9:15 AM
 */

namespace App\Repos;
use App\Comment;


class CommentRepo {

    public function store($request, $idea_id)
    {
        Comment::create([
            'comment' => $request->comment,
            'user_id' => \Auth::user()->id,
            'idea_id' => $idea_id
        ]);
    }

    public function forAnIdea($idea_id){

        return Comment::where('idea_id', $idea_id)->get();
    }

    public function show($idea_id){

        return Comment::where('idea_id', '=', $idea_id)->orderBy('created_at', 'desc')->first();
    }

    public function destroy($comment_id){

        $comment =Comment::findOrFail($comment_id);

        $comment->delete();
    }

} 